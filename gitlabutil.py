# coding:utf-8
'''
用于上传存储过程以及本地文件到gitlab中
'''

import gitlab
import subprocess
import json
import requests
import time
import os


def projectinfo(gl, pid):
    projects = gl.projects.get(pid)
    return projects
    # print projects.name, projects.http_url_to_repo


# 获得project下单个文件   README.md
def getContent(pid):
    projects = projectinfo(pid)
    # 获得文件
    f = projects.files.get(file_path='code/shell/export.sh', ref='main')
    # 第一次decode获得bytes格式的内容
    content = f.decode()
    # # 存到本地
    with open('export.sh', 'wb') as code:
        code.write(content)


def uploadeFile(pid):
    projects = projectinfo(pid)
    # projects.upload("export.sh", filepath="xxxxxx")
    # 这个方法上传文件限制很大 ， file_path 文件名   f.content 用于生成文件内容 ;一定要用 base64 
    f = projects.files.create({'file_path': 'export.sh',
                               'branch': 'main',
                               'content': 'test',
                               'author_email': 'xxx',
                               'author_name': 'xxx',
                               'encoding': 'base64',
                               'commit_message': 'Create export.sh'})
    # 将数据写入
    f.content = open('insert_channel_basic.sql').read()
    f.save(branch='main', commit_message='Update testfile')


# 【实际中用到这个方法】多文件提交参考 : https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
def commit_file(projects, data):
    projects.commits.create(data)


def judgeType(projects, file_path):
    try:
        projects.files.get(file_path=file_path, ref='main')
        return True
    except Exception as e:
        return False


# jMap 用于生成最后的对象; path 文件路径;  jsonList用于生成中间对象
def getData(path, projects, gitlab_source_dir, message):
    jsonList = []
    jMap = {}
    jMap["branch"] = 'main'
    jMap["commit_message"] = message
    for file in os.listdir(path):
        file_dir = os.path.join(path, file)
        # 判断当前路径是文件还是目录 ,排除 pyc造成的编译问题
        if os.path.isfile(file_dir) and 'pyc' not in file:
            gitlab_dir = '%s%s' % (gitlab_source_dir, file)
            aMap = {}
            # 存在就更新， 不存在就创建
            if judgeType(projects, gitlab_dir):
                aMap["action"] = "update"
            else:
                aMap["action"] = "create"
            aMap["file_path"] = gitlab_dir
            aMap["content"] = open(file_dir).read()
            jsonList.append(aMap)
    jMap["actions"] = jsonList
    return jMap


def gitlabupdatehosts():
    ## login
    gl = gitlab.Gitlab.from_config('gitlab', ['.python-gitlab.cfg'])

    # 用于将gitlab上面文件写入本地
    # getContent(87)

    # 1. 将mysql存储过程通过shell脚本写入到指定目录
    try:
        pid = 41203626
        # 获取对应project 对象
        projects = projectinfo(gl, pid)
        # 用于存放拼接好的对象, 如果文件最开始不存在，用update 会报错
        jsonList = []
        jMap = {}
        jMap["branch"] = 'main'
        jMap["commit_message"] = 'update zjsn hosts'
        filePath = '/home/snsoft/sndata/writeHosts/hosts'
        gitlabPath = 'hosts'
        aMap = {}
        # 存在就更新， 不存在就创建
        if judgeType(projects, gitlabPath):
           aMap["action"] = "update"
        else:
           aMap["action"] = "create"
        aMap["file_path"] = gitlabPath
        aMap["content"] = open(filePath).read()
        jsonList.append(aMap)    
        jMap["actions"] = jsonList
        # 用于存储过程的同步
        commit_file(projects, jMap)
        print(f'更新完成')

    except Exception as e:
        nowTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        print(f"【更新hosts同步失败】:【" + nowTime + "】\n" + "【错误原因】:\n" + str(e))