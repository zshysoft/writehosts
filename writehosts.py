#!/usr/bin/python
# -*- coding: UTF-8 -*-
from urllib import request
import re, json, os, logging
import logger, gitlabutil

Headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    # 'Content-type': 'application/x-www-form-urlencoded',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.57'
    }

#获取networkIP
def getNetworkIP():
    #print(f'getworkIP ....')
    ipDict = dict()
    #ip38(ipDict)
    #ip138(ipDict)
    #ipcn(ipDict)
    #print(f'getworkIP ....ipcn.........')
    ip42(ipDict)
    #print(f'getworkIP ....ip42.........')
    #jsonip(ipDict)
    #print(f'getworkIP ....jsonip.........')
    httpbin(ipDict)
    #print(f'getworkIP ....httpbin.........')
    ipify(ipDict)
    #print(f'getworkIP ....ipify.........')
    networkIP = sorted(ipDict.items(), key=lambda d:d[1], reverse = True)[0][0]
    print(f'networkIP is {networkIP}')
    return networkIP

#获取curIP
def getCurIP(ipfilepath):
    ipfile = open(ipfilepath, 'r')
    curIP = ipfile.read()
    ipfile.close()
    print(f'curIP is {curIP}')
    return curIP

#写入hosts
def writeHosts(ipfilepath, hostfilepath, ip):
    ipfile = open(ipfilepath, 'w')
    ipfile.write(ip)
    ipfile.close()
    hostfile = open(hostfilepath, 'w')
    hostfile.write('# ----------------------浙江服务器IP---------------------------\n\n')
    hostfile.write('#centos\n')
    hostfile.write(ip + ' centos.snsoft.work\n\n')
    hostfile.write('#win server\n')
    hostfile.write(ip + ' win.snsoft.work\n\n')
    hostfile.write('#公共\n')
    hostfile.write(ip + ' snsoft.work\n\n')
    hostfile.write('#www \n')
    hostfile.write(ip + ' www.snsoft.work\n\n')
    hostfile.write('#zjsn \n')
    hostfile.write(ip + ' zjsn.snsoft.work\n\n')
    hostfile.write('# ----------------------浙江服务器IP---------------------------')
    hostfile.close()
    hostfile = open(hostfilepath, 'r')
    content = hostfile.read()
    print(content)

# ip38
def ip38(ipDict):
    try:
        ip38Req = request.Request(url=f'http://ip38.com/', headers=Headers, method='GET')
        ip38Res = request.urlopen(ip38Req).read().decode('utf-8')
        ip38 = re.findall(re.compile(r'<a href=/ip.php\?ip=(.*?)>'), ip38Res)[0]
        ipDict[ip38] = ipDict.setdefault(ip38, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# ip138
def ip138(ipDict):
    try:
        ip138Req = request.Request(url=f'https://2021.ip138.com/', headers=Headers, method='GET')
        ip138Res = request.urlopen(ip138Req).read().decode('utf-8')
        ip138 = re.findall(re.compile(r'\[<a.*?>(.*?)</a>\]'), ip138Res)[0]
        ipDict[ip138] = ipDict.setdefault(ip138, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# ipcn
def ipcn(ipDict):
    try:
        ipcnReq = request.Request(url=f'https://ip.cn/api/index?ip=&type=0', headers=Headers, method='GET')
        ipcn = json.loads(request.urlopen(ipcnReq).read().decode('utf-8'))['ip']
        ipDict[ipcn] = ipDict.setdefault(ipcn, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# ip.42
def ip42(ipDict):
    try:
        ip42 = request.urlopen('http://ip.42.pl/raw').read().decode('utf-8')
        ipDict[ip42] = ipDict.setdefault(ip42, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# jsonip
def jsonip(ipDict):
    try:
        jsonip = json.loads(request.urlopen('http://jsonip.com').read().decode('utf-8'))['ip']
        ipDict[jsonip] = ipDict.setdefault(jsonip, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# httpbin
def httpbin(ipDict):
    try:
        httpbin = json.loads(request.urlopen('http://httpbin.org/ip').read().decode('utf-8'))['origin']
        ipDict[httpbin] = ipDict.setdefault(httpbin, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# ipify
def ipify(ipDict):
    try:
        ipify = json.loads(request.urlopen('https://api.ipify.org/?format=json').read().decode('utf-8'))['ip']
        ipDict[ipify] = ipDict.setdefault(ipify, 0) + 1
    except Exception as e:
        logging.error(e)
        pass

# ip-api
def ipapi(ipDict):
    try:
        ipapi = json.loads(request.urlopen('http://ip-api.com/json').read().decode('utf-8'))['query']
        ipDict[ipapi] = ipDict.setdefault(ipapi, 0) + 1
    except Exception as e:
        logging.error(e)
        pass


if __name__ == '__main__':

    logger.setup_logging()
    conf = json.load(open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "conf.json"), "r"))
    ipfilepath = conf['ipfilepath']
    hostfilepath = conf['hostfilepath']
    try:
        curIp = getCurIP(ipfilepath)
        networkIP = getNetworkIP()
        if curIp != networkIP :
            writeHosts(ipfilepath, hostfilepath, networkIP)
            gitlabutil.gitlabupdatehosts()
    except Exception as e:
        logging.error(e)
        pass
